package routes

import (
	"scraper/api/handlers"

	"github.com/gorilla/mux"
)

// NewRouter starts a Mux Router with all routes.
func NewRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/scrape", handlers.Scrape).Methods("POST")
	r.HandleFunc("/multiscrape", handlers.MultiScrape).Methods("POST")
	return r
}
