package handlers

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestScrape(t *testing.T) {
	expected := fmt.Sprintf("%v%v", `{"Username":"Chef Baxter","Name":"Victor Baxter, Cory in the house's father.","Description":"No information given.","GamesTotal":3,"Avatar":"https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/23/23d2a10dde286a70a6d295c4e8b322661cdcc884_full.jpg","Level":4,"Groups":[{"Name":"I Get Mad and Rage After Falling Off Ledges With Chargers","URL":"https://steamcommunity.com/groups/LOLBad","Members":13}],"URL":"https://steamcommunity.com/profiles/76561198119692441","Friends":[{"Username":"13 Dead End Drive","URL":"https://steamcommunity.com/profiles/76561198263389541"},{"Username":"Chicken","URL":"https://steamcommunity.com/id/drogk"},{"Username":"Ape Player","URL":"https://steamcommunity.com/profiles/76561198044940472"},{"Username":"cory Baxter","URL":"https://steamcommunity.com/profiles/76561198004568641"},{"Username":"Doudoume","URL":"https://steamcommunity.com/id/doudoume"},{"Username":"Dr. 420blazeit","URL":"https://steamcommunity.com/profiles/76561198122896165"},{"Username":"Michelle","URL":"https://steamcommunity.com/profiles/76561197988909296"},{"Username":"Playing Mantis","URL":"https://steamcommunity.com/id/liubrian"},{"Username":"SpineSmack","URL":"https://steamcommunity.com/profiles/76561198055752433"}],"Games":null}`, "\n")
	req, err := http.NewRequest("POST", "/scrape", strings.NewReader("https://steamcommunity.com/profiles/76561198119692441"))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Scrape)
	handler.ServeHTTP(rr, req)
	actual := rr.Body.String()
	assert.Equal(t, expected, actual)
}

func TestMultiScrape(t *testing.T) {
	expected := fmt.Sprintf("%v%v", `[{"Username":"Chef Baxter","Name":"Victor Baxter, Cory in the house's father.","Description":"No information given.","GamesTotal":3,"Avatar":"https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/23/23d2a10dde286a70a6d295c4e8b322661cdcc884_full.jpg","Level":4,"Groups":[{"Name":"I Get Mad and Rage After Falling Off Ledges With Chargers","URL":"https://steamcommunity.com/groups/LOLBad","Members":13}],"URL":"https://steamcommunity.com/profiles/76561198119692441","Friends":[{"Username":"13 Dead End Drive","URL":"https://steamcommunity.com/profiles/76561198263389541"},{"Username":"Chicken","URL":"https://steamcommunity.com/id/drogk"},{"Username":"Ape Player","URL":"https://steamcommunity.com/profiles/76561198044940472"},{"Username":"cory Baxter","URL":"https://steamcommunity.com/profiles/76561198004568641"},{"Username":"Doudoume","URL":"https://steamcommunity.com/id/doudoume"},{"Username":"Dr. 420blazeit","URL":"https://steamcommunity.com/profiles/76561198122896165"},{"Username":"Michelle","URL":"https://steamcommunity.com/profiles/76561197988909296"},{"Username":"Playing Mantis","URL":"https://steamcommunity.com/id/liubrian"},{"Username":"SpineSmack","URL":"https://steamcommunity.com/profiles/76561198055752433"}],"Games":null},{"Username":"Chef Baxter","Name":"Victor Baxter, Cory in the house's father.","Description":"No information given.","GamesTotal":3,"Avatar":"https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/23/23d2a10dde286a70a6d295c4e8b322661cdcc884_full.jpg","Level":4,"Groups":[{"Name":"I Get Mad and Rage After Falling Off Ledges With Chargers","URL":"https://steamcommunity.com/groups/LOLBad","Members":13}],"URL":"https://steamcommunity.com/profiles/76561198119692441","Friends":[{"Username":"13 Dead End Drive","URL":"https://steamcommunity.com/profiles/76561198263389541"},{"Username":"Chicken","URL":"https://steamcommunity.com/id/drogk"},{"Username":"Ape Player","URL":"https://steamcommunity.com/profiles/76561198044940472"},{"Username":"cory Baxter","URL":"https://steamcommunity.com/profiles/76561198004568641"},{"Username":"Doudoume","URL":"https://steamcommunity.com/id/doudoume"},{"Username":"Dr. 420blazeit","URL":"https://steamcommunity.com/profiles/76561198122896165"},{"Username":"Michelle","URL":"https://steamcommunity.com/profiles/76561197988909296"},{"Username":"Playing Mantis","URL":"https://steamcommunity.com/id/liubrian"},{"Username":"SpineSmack","URL":"https://steamcommunity.com/profiles/76561198055752433"}],"Games":null},{"Username":"Chef Baxter","Name":"Victor Baxter, Cory in the house's father.","Description":"No information given.","GamesTotal":3,"Avatar":"https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/23/23d2a10dde286a70a6d295c4e8b322661cdcc884_full.jpg","Level":4,"Groups":[{"Name":"I Get Mad and Rage After Falling Off Ledges With Chargers","URL":"https://steamcommunity.com/groups/LOLBad","Members":13}],"URL":"https://steamcommunity.com/profiles/76561198119692441","Friends":[{"Username":"13 Dead End Drive","URL":"https://steamcommunity.com/profiles/76561198263389541"},{"Username":"Chicken","URL":"https://steamcommunity.com/id/drogk"},{"Username":"Ape Player","URL":"https://steamcommunity.com/profiles/76561198044940472"},{"Username":"cory Baxter","URL":"https://steamcommunity.com/profiles/76561198004568641"},{"Username":"Doudoume","URL":"https://steamcommunity.com/id/doudoume"},{"Username":"Dr. 420blazeit","URL":"https://steamcommunity.com/profiles/76561198122896165"},{"Username":"Michelle","URL":"https://steamcommunity.com/profiles/76561197988909296"},{"Username":"Playing Mantis","URL":"https://steamcommunity.com/id/liubrian"},{"Username":"SpineSmack","URL":"https://steamcommunity.com/profiles/76561198055752433"}],"Games":null}]`, "\n")
	req, err := http.NewRequest("POST", "/multiscrape", strings.NewReader(`["https://steamcommunity.com/profiles/76561198119692441","https://steamcommunity.com/profiles/76561198119692441","https://steamcommunity.com/profiles/76561198119692441"]`))
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(MultiScrape)
	handler.ServeHTTP(rr, req)
	actual := rr.Body.String()
	assert.Equal(t, expected, actual)
}
