package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"scraper/pkg/models/profile"
	"scraper/src/scraper"
)

func handlerError(w http.ResponseWriter, handler string, err error) {
	log.Printf("%v - Error: %v", handler, err)
	json.NewEncoder(w).Encode(fmt.Sprintf(`{error: '%v'}`, err))
}

func HandleErrors(pkg string, function string, err error) {
	log.Printf("%v-%v - Error: %v", pkg, function, err)
}

// Scrape takes a URL string to call scraper.GetData() followed by scraper.ScrapeProfile() and ScrapeFriends.
func Scrape(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	data, err := scraper.GetData(string(body))
	if err != nil {
		handlerError(w, "Scrape", err)
		return
	}
	scrapedData := scraper.ScrapeProfile(data.Body)
	data, err = scraper.GetData(fmt.Sprintf("%v%v", scrapedData.URL, "/friends"))
	if err != nil {
		handlerError(w, "Scrape", err)
		return
	}
	scrapedData = scraper.ScrapeFriends(data.Body, scrapedData)
	data, err = scraper.GetData(fmt.Sprintf("%v%v", scrapedData.URL, "/groups"))
	if err != nil {
		handlerError(w, "Scrape", err)
		return
	}
	scrapedData, err = scraper.ScrapeGroups(data.Body, scrapedData)
	if err != nil {
		handlerError(w, "Scrape", err)
		return
	}
	data, err = scraper.GetData(fmt.Sprintf("%v%v", scrapedData.URL, "/games/?tab=all"))
	if err != nil {
		handlerError(w, "Scrape", err)
		return
	}
	scrapedData, err = scraper.ScrapeGames(data.Body, scrapedData)
	if err != nil {
		handlerError(w, "Scrape", err)
		return
	}
	json.NewEncoder(w).Encode(scrapedData)
}

// MultiScrape takes a URL array to call scraper.GetData() followed by scraper.ScrapeProfile() and scraper.ScrapeFriends().
func MultiScrape(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var urlArray []string
	err := json.Unmarshal(body, &urlArray)
	if err != nil {
		handlerError(w, "MultiScrape", err)
		return
	}
	var scrapedDataArray []profile.Profile
	for _, v := range urlArray {
		data, err := scraper.GetData(string(v))
		if err != nil {
			handlerError(w, "MultiScrape", err)
			return
		}
		scrapedData := scraper.ScrapeProfile(data.Body)
		data, err = scraper.GetData(fmt.Sprintf("%v%v", scrapedData.URL, "/friends"))
		if err != nil {
			handlerError(w, "MultiScrape", err)
			return
		}
		scrapedData = scraper.ScrapeFriends(data.Body, scrapedData)
		data, err = scraper.GetData(fmt.Sprintf("%v%v", scrapedData.URL, "/groups"))
		if err != nil {
			handlerError(w, "Scrape", err)
			return
		}
		scrapedData, err = scraper.ScrapeGroups(data.Body, scrapedData)
		if err != nil {
			handlerError(w, "Scrape", err)
			return
		}
		scrapedData, err = scraper.ScrapeGames(data.Body, scrapedData)
		if err != nil {
			handlerError(w, "Scrape", err)
			return
		}
		scrapedDataArray = append(scrapedDataArray, scrapedData)

	}
	json.NewEncoder(w).Encode(scrapedDataArray)
}
