# Steam-Scraper

An API that takes in steam profile URLs and returns the available profile data in a JSON format.

## Available Data:

    - Username
    - Name
    - Description
    - Profile URL
    - Avatar Image URL
    - Level
    - Number of Games
    - Groups
        - Name 
        - Group URL
    - Friends
        - Name
        - Friend URL
    - Games
        - Name
        - Game URL
        - Image URL
        - Recent Hours
        - Total Hours
        - Game URL
        - Last Played Date

## Install

    - $ brew install dep
    - $ brew upgrade dep
    - $ dep ensure -v
    - $ go run main.go

## End Points

### `POST /scrape`
Takes a steam profile URL and returns the profile's data in a JSON format.
    
    INPUT:
    POST http://localhost:8000/scrape

    https://steamcommunity.com/id/Rina

    OUTPUT:
    {
        "Username": "Rina",
        "Name": "",
        "Description": "No information given.",
        "GamesTotal": 2,
        "Avatar": "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/30/305caf97c2085cc231aa354a0613012408e8ad82_full.jpg",
        "Level": 11,
        "Groups": [
            {
                "Name": "GamerCave",
                "URL": "https://steamcommunity.com/groups/bilibi999",
                "Members": 12327
            }
        ],
        "URL": "https://steamcommunity.com/profiles/76561198325581999",
        "Friends": [
            {
                "Username": "Gery丶",
                "URL": "https://steamcommunity.com/id/ngkwok999"
            },
            {
                "Username": "HanBeans",
                "URL": "https://steamcommunity.com/profiles/76561198144819999"
            },
            {
                "Username": "HESM",
                "URL": "https://steamcommunity.com/profiles/76561198328962999"
            }
        ],
        "Games": [
            {
                "AppID": 730,
                "Name": "Counter-Strike: Global Offensive",
                "Logo": "https://steamcdn-a.akamaihd.net/steam/apps/730/capsule_184x69.jpg",
                "AdultContent": 1,
                "Stats": {
                    "Achievements": true,
                    "GlobalAchievements": true,
                    "Stats": false,
                    "GCPD": true,
                    "Leaderboards": false,
                    "GlobalLeaderboards": false
                },
                "HoursRecent": "6.4",
                "HoursTotal": "1,113",
                "LastPlayed": 1577660358,
                "URL": "https://store.steampowered.com/app/730"
            },
            {
                "AppID": 374320,
                "Name": "DARK SOULS™ III",
                "Logo": "https://steamcdn-a.akamaihd.net/steam/apps/374320/capsule_184x69.jpg",
                "AdultContent": 0,
                "Stats": {
                    "Achievements": true,
                    "GlobalAchievements": true,
                    "Stats": false,
                    "GCPD": false,
                    "Leaderboards": false,
                    "GlobalLeaderboards": false
                },
                "HoursRecent": "",
                "HoursTotal": "179",
                "LastPlayed": 1568718677,
                "URL": "https://store.steampowered.com/app/374320"
            }
        ]
    }

### `POST /multiscrape`
Takes an array of steam profile URL strings and returns an array of profile data in a JSON format.
    
    INPUT:
    POST http://localhost:8000/multiscrape

    ["https://steamcommunity.com/id/Rina", "https://steamcommunity.com/id/Carson"]

    OUTPUT:
    [
        {
            "Username": "Rina",
            "Name": "",
            "Description": "No information given.",
            "GamesTotal": 2,
            "Avatar": "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/30/305caf97c2085cc231aa354a0613012408e8ad82_full.jpg",
            "Level": 11,
            "Groups": [
                {
                    "Name": "GamerCave",
                    "URL": "https://steamcommunity.com/groups/bilibi999",
                    "Members": 12327
                }
            ],
            "URL": "https://steamcommunity.com/profiles/76561198325581999",
            "Friends": [
                ...
            ],
            "Games": [
            ...
            ]
        },
        {
            "Username": "Carson",
            "Name": "",
            "Description": "No information given.",
            "GamesTotal": 5,
            "Avatar": "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/30/305caf97c2085cc231aa354a0613012408e8ad82_full.jpg",
            "Level": 19,
            "Groups": [
                {
                    "Name": "GamerCave",
                    "URL": "https://steamcommunity.com/groups/bilibi999",
                    "Members": 12327
                }
            ],
            "URL": "https://steamcommunity.com/profiles/76561198325581999",
            "Friends": [
                ...
            ],
            "Games": [
                ...
            ]
        }
    ]