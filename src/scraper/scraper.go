package scraper

import (
	"bufio"
	"io"
	"log"
	"net/http"
	"scraper/pkg/models/friend"
	game "scraper/pkg/models/games"
	"scraper/pkg/models/group"
	"scraper/pkg/models/profile"
)

func GetData(url string) (*http.Response, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("GetData - Error: %v", err)
		return nil, err
	}
	return resp, nil
}

func ScrapeProfile(data io.Reader) (pData profile.Profile) {
	scanner := bufio.NewScanner(data)
	for scanner.Scan() {
		pData.GetUsername(scanner.Text())
		pData.GetDescription(scanner.Text())
		pData.GetGamesTotal(scanner.Text())
		pData.GetAvatar(scanner.Text())
		pData.GetLevel(scanner.Text())
		pData.GetName(scanner.Text())
		pData.GetURL(scanner.Text())
	}
	return pData
}

func ScrapeFriends(data io.Reader, pData profile.Profile) profile.Profile {
	var fData friend.Friend
	scanner := bufio.NewScanner(data)
	for scanner.Scan() {
		if fData.NonEmpty() {
			pData.Friends = append(pData.Friends, fData)
			fData = friend.New()
		}
		fData.GetFriends(scanner.Text())
	}
	return pData
}

func ScrapeGroups(data io.Reader, pData profile.Profile) (profile.Profile, error) {
	var gData group.Group
	scanner := bufio.NewScanner(data)
	for scanner.Scan() {
		if gData.NonEmpty() {
			pData.Groups = append(pData.Groups, gData)
			gData = group.New()
		}
		err := gData.GetGroups(scanner.Text())
		if err != nil {
			log.Printf("%v-%v - Error: %v", "scraper", "ScrapeGroups", err)
			return pData, err
		}
	}
	return pData, nil
}

func ScrapeGames(data io.Reader, pData profile.Profile) (profile.Profile, error) {
	scanner := bufio.NewScanner(data)
	for scanner.Scan() {
		if len(pData.Games) == 0 {
			var err error
			pData.Games, err = game.GetGames(scanner.Text())
			if err != nil {
				log.Printf("%v-%v - Error: %v", "scraper", "ScrapeGames", err)
				return pData, err
			}
		}
	}
	return pData, nil
}
