package scraper

import (
	"fmt"
	"os"
	"scraper/pkg/models/friend"
	game "scraper/pkg/models/games"
	"scraper/pkg/models/group"
	"scraper/pkg/models/profile"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestScrapeProfile(t *testing.T) {
	cases := []string{"bariki", "mufasa", "sr", "sunai"}
	expected := []profile.Profile{
		{
			Username:    "Bariki",
			Description: "No information given.",
			GamesTotal:  38,
			Avatar:      "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/68/6842e5e9be216068f88241e88a70f0c6a16a0565_full.jpg",
			Level:       10,
			Name:        "Bariki",
			URL:         "https://steamcommunity.com/id/bariki",
			Groups:      nil,
			Games:       nil,
		},
		{
			Username:    "mufasa",
			Description: "Welcome to my desolate profile :^) enjoy your stay!",
			GamesTotal:  133,
			Avatar:      "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/1f/1f4832f2e233de9b89480a1538941013e17edd59_full.jpg",
			Level:       89,
			Name:        "Silence",
			URL:         "https://steamcommunity.com/id/firejumper",
			Groups:      nil,
			Games:       nil,
		},
		{
			Username:    "SR",
			Description: "No information given.",
			GamesTotal:  261,
			Avatar:      "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/90/90491d1ca276269843fdcd342f02a27b45c03cdf_full.jpg",
			Level:       176,
			Name:        "Cetin Dixon",
			URL:         "https://steamcommunity.com/id/SprintlineR",
			Groups:      nil,
			Games:       nil,
		},
		{
			Username:    "Sunai",
			Description: "Herhangi bir bilgi verilmedi.",
			GamesTotal:  89,
			Avatar:      "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/84/845082e658afd9d96520cae70acbebf44d4f75e0_full.jpg",
			Level:       43,
			Name:        "",
			URL:         "https://steamcommunity.com/profiles/76561198307323921",
			Groups:      nil,
			Games:       nil,
		},
	}
	for i, v := range cases {
		file, err := os.Open(fmt.Sprintf("./../../fixtures/profile_data/%v.txt", v))
		actual := ScrapeProfile(file)
		if err != nil {
			t.Errorf("Failed")
		}
		assert.Equal(t, expected[i], actual)
	}
}

func TestScrapeFriends(t *testing.T) {
	cases := []string{"bariki", "mufasa"}
	expected := []profile.Profile{
		{Friends: []friend.Friend{{Username: "Slippery Opossum", URL: "https://steamcommunity.com/id/GordonBunnyFreeman"}, {Username: "ArtofZod @ Twitch", URL: "https://steamcommunity.com/id/artofzod"}, {Username: "Awkore", URL: "https://steamcommunity.com/id/Awkore"}, {Username: "Funky", URL: "https://steamcommunity.com/id/funkyf0x"}, {Username: "Reverb Skyriff", URL: "https://steamcommunity.com/id/macenziewolf"}, {Username: "Scix", URL: "https://steamcommunity.com/id/scix501"}, {Username: "WashuDriger", URL: "https://steamcommunity.com/id/washuchan"}, {Username: "adrian gomez", URL: "https://steamcommunity.com/profiles/76561198002696102"}, {Username: "Agent Johnson{#TURTLEFOOT!}", URL: "https://steamcommunity.com/profiles/76561197988692963"}, {Username: "Ammy", URL: "https://steamcommunity.com/id/amethystmare"}, {Username: "Badge", URL: "https://steamcommunity.com/id/thebluebadger"}, {Username: "Bastett", URL: "https://steamcommunity.com/id/bastett"}, {Username: "beanis", URL: "https://steamcommunity.com/id/dinglehopper"}, {Username: "BeshonCat", URL: "https://steamcommunity.com/id/beshon"}, {Username: "Biffa ᵀᴼᴬ mic on !!", URL: "https://steamcommunity.com/profiles/76561197969037642"}, {Username: "Blackrose", URL: "https://steamcommunity.com/id/ArchBlackrose"}, {Username: "Blue", URL: "https://steamcommunity.com/profiles/76561197995727073"}, {Username: "Bolly!", URL: "https://steamcommunity.com/id/bollythewolf"}, {Username: "Branston", URL: "https://steamcommunity.com/id/branston"}, {Username: "Bungle McBear", URL: "https://steamcommunity.com/id/bungle_bearskunk"}, {Username: "CaballoLeche", URL: "https://steamcommunity.com/id/donkeymissile"}, {Username: "CMDR Ren Nova", URL: "https://steamcommunity.com/id/renamix6"}, {Username: "Cola", URL: "https://steamcommunity.com/id/cola_katz"}, {Username: "Cosmo", URL: "https://steamcommunity.com/profiles/76561198024927443"}, {Username: "Cueball", URL: "https://steamcommunity.com/profiles/76561197990058046"}, {Username: "effowe", URL: "https://steamcommunity.com/id/effowe"}, {Username: "EnglishDragoon", URL: "https://steamcommunity.com/id/englishdragoon"}, {Username: "Evil Bear", URL: "https://steamcommunity.com/profiles/76561197996484079"}, {Username: "Freddy Panda", URL: "https://steamcommunity.com/id/pandafreddy"}, {Username: "Gavdoff", URL: "https://steamcommunity.com/id/Gavdoff"}, {Username: "Hozzie", URL: "https://steamcommunity.com/id/hozziehozzbourne"}, {Username: "HyPerRifiC", URL: "https://steamcommunity.com/id/Hyperrificail"}, {Username: "Inanimatt", URL: "https://steamcommunity.com/id/inanimatt"}, {Username: "in position ◕‿◕", URL: "https://steamcommunity.com/profiles/76561198014816039"}, {Username: "J-ʕ•ᴥ•ʔ", URL: "https://steamcommunity.com/id/ElectroLoki"}, {Username: "Jonesy319", URL: "https://steamcommunity.com/id/Jonesy319"}, {Username: "Jury Nullification", URL: "https://steamcommunity.com/profiles/76561198008427236"}, {Username: "Kamira", URL: "https://steamcommunity.com/id/KamiraHusky"}, {Username: "Kenji / Pulse", URL: "https://steamcommunity.com/id/_kenj1"}, {Username: "Kiteless [oMc]", URL: "https://steamcommunity.com/id/kiteless"}, {Username: "Krute-Loops", URL: "https://steamcommunity.com/profiles/76561197999098409"}, {Username: "Lazerus101", URL: "https://steamcommunity.com/id/Lazerus101"}, {Username: "LeoSnowmew#2498 4 Overwatch", URL: "https://steamcommunity.com/id/Belgiansnep"}, {Username: "MetalCrisp", URL: "https://steamcommunity.com/id/shadowspawner"}, {Username: "Noha", URL: "https://steamcommunity.com/id/Noha_Kitsune"}, {Username: "PanKat", URL: "https://steamcommunity.com/profiles/76561198027381007"}, {Username: "Party Fox", URL: "https://steamcommunity.com/id/PartyFox"}, {Username: "Pippin The Floofy", URL: "https://steamcommunity.com/profiles/76561198025175203"}, {Username: "Rega", URL: "https://steamcommunity.com/id/rega128"}, {Username: "Renbymon", URL: "https://steamcommunity.com/id/graafenb"}, {Username: "Rikev", URL: "https://steamcommunity.com/id/rikev"}, {Username: "Rikka 🐱\u200d🐉", URL: "https://steamcommunity.com/id/rikka_hyou"}, {Username: "Sacred", URL: "https://steamcommunity.com/id/sacredbull"}, {Username: "Senji", URL: "https://steamcommunity.com/id/Senjiroh"}, {Username: "SethWolfbaine", URL: "https://steamcommunity.com/id/SethWolfbaine"}, {Username: "SgtNoah", URL: "https://steamcommunity.com/profiles/76561197999685197"}, {Username: "TheBigD", URL: "https://steamcommunity.com/profiles/76561197981174701"}, {Username: "The Last Raven", URL: "https://steamcommunity.com/profiles/76561198011919459"}, {Username: "Tryst", URL: "https://steamcommunity.com/id/tryst_me"}, {Username: "Trystykat", URL: "https://steamcommunity.com/id/barnzi"}, {Username: "Tungro", URL: "https://steamcommunity.com/id/tungro"}, {Username: "Twll", URL: "https://steamcommunity.com/id/twll"}, {Username: "uliuses", URL: "https://steamcommunity.com/id/SCARvFACE"}, {Username: "VatraLion", URL: "https://steamcommunity.com/profiles/76561198028139539"}, {Username: "Volkatron", URL: "https://steamcommunity.com/profiles/76561197999659628"}, {Username: "Wickaman", URL: "https://steamcommunity.com/profiles/76561197976985212"}, {Username: "Wootz / Sly", URL: "https://steamcommunity.com/id/wootz"}, {Username: "yksi", URL: "https://steamcommunity.com/id/yksi"}, {Username: "[BL] Cheesewheel", URL: "https://steamcommunity.com/id/Cheesewheel_"}, {Username: "[U][A]-Tanzen----&gt;Terribad", URL: "https://steamcommunity.com/profiles/76561197960872787"}, {Username: "]CI[ Exodus", URL: "https://steamcommunity.com/profiles/76561198022873671"}, {Username: "]NME[*TheHunted", URL: "https://steamcommunity.com/profiles/76561197981080304"}}},
		{Friends: []friend.Friend{{Username: "Keo", URL: "https://steamcommunity.com/id/Somrung"}, {Username: "420 sad", URL: "https://steamcommunity.com/id/mountain-dew-voltage"}, {Username: "aFuzzyDinosaur", URL: "https://steamcommunity.com/id/aFuzzyDinosaur"}, {Username: "BKdude99", URL: "https://steamcommunity.com/profiles/76561198066502738"}, {Username: "ColdMiller", URL: "https://steamcommunity.com/id/ColdMiller"}, {Username: "Devadora", URL: "https://steamcommunity.com/profiles/76561198190649829"}, {Username: "EdenSanctum", URL: "https://steamcommunity.com/profiles/76561198240942032"}, {Username: "Gingerlicious1", URL: "https://steamcommunity.com/profiles/76561198029310761"}, {Username: "GodsBum", URL: "https://steamcommunity.com/id/RagingErectionOfDeath"}, {Username: "iAmPonyo", URL: "https://steamcommunity.com/profiles/76561198097757732"}, {Username: "it me", URL: "https://steamcommunity.com/id/Ashbringer874"}, {Username: "Madcap", URL: "https://steamcommunity.com/id/trappucap"}, {Username: "pie19988", URL: "https://steamcommunity.com/id/pie19988"}, {Username: "Rivers Cuomo", URL: "https://steamcommunity.com/profiles/76561198041503010"}, {Username: "sarcastic6", URL: "https://steamcommunity.com/id/sarcastic6"}, {Username: "ShuFFleZombie09", URL: "https://steamcommunity.com/id/ShuFFleZombie09"}, {Username: "Squidward", URL: "https://steamcommunity.com/profiles/76561198135387965"}, {Username: "SuhhWayy", URL: "https://steamcommunity.com/id/MaverickShirt"}, {Username: "Wolf Pack  [B.$.C] Slasher", URL: "https://steamcommunity.com/id/slasher519"}, {Username: "🐱 Davarice", URL: "https://steamcommunity.com/id/Davarice"}, {Username: "Aelian", URL: "https://steamcommunity.com/profiles/76561197983752803"}, {Username: "Alienaura", URL: "https://steamcommunity.com/profiles/76561198122522912"}, {Username: "BAD | Youtubehype", URL: "https://steamcommunity.com/id/youtubehype"}, {Username: "Bean Soup", URL: "https://steamcommunity.com/id/SoVerySleepy"}, {Username: "Bricson", URL: "https://steamcommunity.com/id/beaverbc"}, {Username: "Broseidon Ruler Of The Brocean", URL: "https://steamcommunity.com/id/BroceanRuler"}, {Username: "Chris", URL: "https://steamcommunity.com/id/OneWingedAngel43"}, {Username: "devilkingxam", URL: "https://steamcommunity.com/profiles/76561198057587214"}, {Username: "dustybivins", URL: "https://steamcommunity.com/profiles/76561198110588165"}, {Username: "Enii.ko", URL: "https://steamcommunity.com/id/Eniiko"}, {Username: "Farnadir", URL: "https://steamcommunity.com/id/Farnadir"}, {Username: "IDK WHO B DIS", URL: "https://steamcommunity.com/profiles/76561198280898163"}, {Username: "Im@gine", URL: "https://steamcommunity.com/profiles/76561198199176575"}, {Username: "isometricramen", URL: "https://steamcommunity.com/id/isometricramen"}, {Username: "jollyredrum", URL: "https://steamcommunity.com/profiles/76561198799203498"}, {Username: "Kaiden47", URL: "https://steamcommunity.com/id/Kaiden47"}, {Username: "Kate", URL: "https://steamcommunity.com/profiles/76561198239540152"}, {Username: "Mami Penguin", URL: "https://steamcommunity.com/id/Penguin9078"}, {Username: "MarineSloth", URL: "https://steamcommunity.com/id/marinesloth"}, {Username: "neku", URL: "https://steamcommunity.com/id/NekuART"}, {Username: "Noodle", URL: "https://steamcommunity.com/profiles/76561198142773939"}, {Username: "Ostara", URL: "https://steamcommunity.com/id/The-Creamy-Meme"}, {Username: "Peppermint Pete", URL: "https://steamcommunity.com/id/BigMommaThunderThighs"}, {Username: "Questionable Soviet Ethics", URL: "https://steamcommunity.com/id/1998Vincent1998"}, {Username: "Rattalyn", URL: "https://steamcommunity.com/id/Rattalyn"}, {Username: "Runnin' n gunnin' since 89'", URL: "https://steamcommunity.com/id/JaBrEaD"}, {Username: "Shady's bot", URL: "https://steamcommunity.com/id/BandAidDeagle"}, {Username: "Solayr", URL: "https://steamcommunity.com/id/dumbfsckingurlgarbage"}, {Username: "Tehpwnagepie", URL: "https://steamcommunity.com/profiles/76561198045417083"}, {Username: "urmom", URL: "https://steamcommunity.com/id/amweebtrash"}, {Username: "Waaa", URL: "https://steamcommunity.com/id/uwuyeto"}, {Username: "waffre", URL: "https://steamcommunity.com/id/kwakkevin"}, {Username: "WizardOfOruz", URL: "https://steamcommunity.com/id/Oruz"}, {Username: "Xandra", URL: "https://steamcommunity.com/id/xandramaree"}, {Username: "yaboipanda", URL: "https://steamcommunity.com/id/Kill_me_god"}, {Username: "YEA BABY YEA", URL: "https://steamcommunity.com/profiles/76561198221867504"}, {Username: "yoshifodder", URL: "https://steamcommunity.com/profiles/76561198073380663"}, {Username: "[LGS] God of Conquest", URL: "https://steamcommunity.com/profiles/76561198110873804"}, {Username: "[PG] Galahir950", URL: "https://steamcommunity.com/id/Galahir950"}, {Username: "𝕵𝖊𝖘𝖙𝖊𝖗", URL: "https://steamcommunity.com/id/dibleton"}, {Username: "💨 Stilly", URL: "https://steamcommunity.com/id/341464"}}},
	}
	for i, v := range cases {
		file, err := os.Open(fmt.Sprintf("./../../fixtures/profile_data/%v_friends.txt", v))
		actual := ScrapeFriends(file, profile.Profile{})
		if err != nil {
			t.Errorf("Failed")
		}
		assert.Equal(t, expected[i], actual)
	}
}

func TestScrapeGroups(t *testing.T) {
	cases := []string{"bariki", "mufasa"}
	expected := []profile.Profile{
		{Groups: []group.Group{{Name: "Lanraiders", URL: "https://steamcommunity.com/groups/lanraiders", Members: 108}, {Name: "Evil Smurfs", URL: "https://steamcommunity.com/groups/evilsmurfs", Members: 712}, {Name: "United's Friends", URL: "https://steamcommunity.com/groups/unitedsfriends", Members: 128}, {Name: "'express-gaming'", URL: "https://steamcommunity.com/groups/originalexpressgaming", Members: 250}}},
		{Groups: []group.Group{{Name: "❆ Anime Galaxy ❆", URL: "https://steamcommunity.com/groups/Anime-Galaxy", Members: 1060}, {Name: "Beyond Single Dimensional", URL: "https://steamcommunity.com/groups/Beyond1D", Members: 8}, {Name: "Floyd's Army", URL: "https://steamcommunity.com/groups/floydsarmy", Members: 1163}, {Name: "Love\u00a0You", URL: "https://steamcommunity.com/groups/lovepeople", Members: 409}, {Name: "Praise Asanagi SenSe", URL: "https://steamcommunity.com/groups/TooEcchiDesune", Members: 147}}},
	}
	for i, v := range cases {
		file, err := os.Open(fmt.Sprintf("./../../fixtures/profile_data/%v_groups.txt", v))
		if err != nil {
			t.Errorf("Failed")
		}
		actual, err := ScrapeGroups(file, profile.Profile{})
		if err != nil {
			t.Errorf("Failed")
		}
		assert.Equal(t, expected[i], actual)
	}
}

func TestScrapeGames(t *testing.T) {
	cases := []string{"booger", "chefbaxter"}
	expected := []profile.Profile{
		{Games: []game.Game{game.Game{AppID: 730, Name: "Counter-Strike: Global Offensive", Logo: "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/730/d0595ff02f5c79fd19b06f4d6165c3fda2372820.jpg", AdultContent: 1, Stats: game.Statistics{Achievements: true, GlobalAchievements: true, Stats: false, GCPD: true, Leaderboards: false, GlobalLeaderboards: false}, HoursRecent: "70.5", HoursTotal: "2,410", LastPlayed: 1573911970, URL: "https://store.steampowered.com/app/730"}, game.Game{AppID: 438100, Name: "VRChat", Logo: "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/438100/62385c4bb53297d192a76829fce5d05b1f28ab90.jpg", AdultContent: 0, Stats: game.Statistics{Achievements: true, GlobalAchievements: true, Stats: false, GCPD: false, Leaderboards: false, GlobalLeaderboards: false}, HoursRecent: "", HoursTotal: "1.9", LastPlayed: 1541828643, URL: "https://store.steampowered.com/app/438100"}, game.Game{AppID: 107410, Name: "Arma 3", Logo: "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/107410/b49962441a01f1f80b180af1293608dddf7df6b0.jpg", AdultContent: 0, Stats: game.Statistics{Achievements: true, GlobalAchievements: true, Stats: false, GCPD: false, Leaderboards: true, GlobalLeaderboards: true}, HoursRecent: "", HoursTotal: "0.9", LastPlayed: 1494625273, URL: "https://store.steampowered.com/app/107410"}, game.Game{AppID: 639600, Name: "Arma 3 Malden", Logo: "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/639600/943606f28068a89ab7f97171fd2ba5c69a502598.jpg", AdultContent: 0, Stats: game.Statistics{Achievements: true, GlobalAchievements: true, Stats: false, GCPD: false, Leaderboards: false, GlobalLeaderboards: false}, HoursRecent: "", HoursTotal: "", LastPlayed: 0, URL: "https://store.steampowered.com/app/639600"}, game.Game{AppID: 275700, Name: "Arma 3 Zeus", Logo: "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/apps/275700/51f031fdec573291d373ad3f6156f6691892c84c.jpg", AdultContent: 0, Stats: game.Statistics{Achievements: true, GlobalAchievements: true, Stats: false, GCPD: false, Leaderboards: false, GlobalLeaderboards: false}, HoursRecent: "", HoursTotal: "", LastPlayed: 0, URL: "https://store.steampowered.com/app/275700"}}},
		{},
	}
	for i, v := range cases {
		file, err := os.Open(fmt.Sprintf("./../../fixtures/profile_data/%v_games.txt", v))
		if err != nil {
			t.Errorf("Failed")
		}
		actual, err := ScrapeGames(file, profile.Profile{})
		if err != nil {
			t.Errorf("Failed")
		}
		assert.Equal(t, expected[i], actual)
	}
}
