package game

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
)

type GameRaw struct {
	AppID        int           `json:"appid"`
	Name         string        `json:"name"`
	Logo         string        `json:"logo"`
	AdultContent int           `json:"has_adult_content"`
	Stats        StatisticsRaw `json:"availStatLinks"`
	HoursRecent  string        `json:"hours"`
	HoursTotal   string        `json:"hours_forever"`
	LastPlayed   int           `json:"last_played"`
	URL          string        `json:"URL"`
}

type Game struct {
	AppID        int        `json:"AppID"`
	Name         string     `json:"Name"`
	Logo         string     `json:"Logo"`
	AdultContent int        `json:"AdultContent"`
	Stats        Statistics `json:"Stats"`
	HoursRecent  string     `json:"HoursRecent"`
	HoursTotal   string     `json:"HoursTotal"`
	LastPlayed   int        `json:"LastPlayed"`
	URL          string     `json:"URL"`
}

type StatisticsRaw struct {
	Achievements       bool `json:"achievements"`
	GlobalAchievements bool `json:"global_achievements"`
	Stats              bool `json:"stats"`
	GCPD               bool `json:"gcpd"`
	Leaderboards       bool `json:"leaderboards"`
	GlobalLeaderboards bool `json:"global_leaderboards"`
}

type Statistics struct {
	Achievements       bool `json:"Achievements"`
	GlobalAchievements bool `json:"GlobalAchievements"`
	Stats              bool `json:"Stats"`
	GCPD               bool `json:"GCPD"`
	Leaderboards       bool `json:"Leaderboards"`
	GlobalLeaderboards bool `json:"GlobalLeaderboards"`
}

func New() Game {
	return Game{}
}

func (g *Game) NonEmpty() bool {
	return g.Name != "" && g.AppID != 0
}

func (g GameRaw) convertToGame() Game {
	return Game{
		AppID:        g.AppID,
		Name:         g.Name,
		Logo:         g.Logo,
		AdultContent: g.AdultContent,
		Stats: Statistics{
			Achievements:       g.Stats.Achievements,
			GlobalAchievements: g.Stats.GlobalAchievements,
			Stats:              g.Stats.Stats,
			GCPD:               g.Stats.GCPD,
			Leaderboards:       g.Stats.Leaderboards,
			GlobalLeaderboards: g.Stats.GlobalLeaderboards,
		},
		HoursRecent: g.HoursRecent,
		HoursTotal:  g.HoursTotal,
		LastPlayed:  g.LastPlayed,
		URL:         fmt.Sprintf("%v%v", "https://store.steampowered.com/app/", g.AppID),
	}
}

func GetGames(data string) ([]Game, error) {
	var gamesArray []Game
	if strings.Contains(data, `var rgGames = [];`) {
		return nil, nil
	} else if strings.Contains(data, `var rgGames =`) {
		gamesDataStr := strings.TrimLeft(data, `			var rgGames = [{`)
		gamesDataStr = strings.TrimRight(gamesDataStr, "];")
		gamesDataArray := strings.Split(gamesDataStr, "},{")
		var rawGamesArray []string
		for i, v := range gamesDataArray {
			if i != len(gamesDataArray)-1 {
				rawGamesArray = append(rawGamesArray, fmt.Sprintf("%v%v%v", "{", v, "}"))
			} else {
				rawGamesArray = append(rawGamesArray, fmt.Sprintf("%v%v", "{", v))
			}
		}
		for _, v := range rawGamesArray {
			var gameRaw GameRaw
			err := json.Unmarshal([]byte(v), &gameRaw)
			if err != nil {
				log.Printf("%v-%v - Error: %v", "game", "GetGames", err)
				return gamesArray, err
			}
			game := gameRaw.convertToGame()
			gamesArray = append(gamesArray, game)
		}
	}
	return gamesArray, nil
}
