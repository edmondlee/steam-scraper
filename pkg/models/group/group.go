package group

import (
	"log"
	"regexp"
	"strconv"
	"strings"
)

type Group struct {
	Name    string
	URL     string
	Members int
}

func New() Group {
	return Group{}
}

func (g *Group) NonEmpty() bool {
	return g.Name != "" && g.URL != "" && g.Members != 0
}

func (g *Group) GetGroups(data string) error {
	if strings.Contains(data, `<a class="linkTitle" href="https://steamcommunity.com/groups`) {
		re := regexp.MustCompile(`[><]`)
		array := re.Split(data, -1)
		g.Name = array[2]
		replacer := strings.NewReplacer(`a class="linkTitle" href="`, "", `"`, "", "	", "")
		g.URL = replacer.Replace(array[1])
	}
	if strings.Contains(data, `<a class="groupMemberStat linkStandard" href=`) {
		re := regexp.MustCompile(`[>]`)
		array := re.Split(data, -1)
		memberStr := strings.TrimRight(array[1], " Members</a")
		replacer := strings.NewReplacer(",", "")
		memberStr = replacer.Replace(memberStr)
		memberInt, err := strconv.Atoi(memberStr)
		if err != nil {
			log.Printf("%v-%v - Error: %v", "group", "GetGroups", err)
			return err
		}
		g.Members = memberInt
	}
	return nil
}
