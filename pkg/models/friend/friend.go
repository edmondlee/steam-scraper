package friend

import "strings"

type Friend struct {
	Username string
	URL      string
}

func New() Friend {
	return Friend{}
}

func (f *Friend) NonEmpty() bool {
	return f.URL != "" && f.Username != ""
}

func (f *Friend) GetFriends(data string) {
	if strings.Contains(data, `<a class="selectable_overlay"`) {
		replacer := strings.NewReplacer(`"></a>`, "")
		array := strings.Split(replacer.Replace(data), `href="`)
		f.URL = replacer.Replace(array[1])
	}
	if strings.Contains(data, `<div class="friend_block_content">`) {
		replacer := strings.NewReplacer(`<div class="friend_block_content">`, "", `<br>`, "", "	", "")
		f.Username = replacer.Replace(data)
	}
}
