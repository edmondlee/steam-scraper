package profile

import (
	"fmt"
	"log"
	"scraper/pkg/models/friend"
	game "scraper/pkg/models/games"
	"scraper/pkg/models/group"
	"strconv"
	"strings"
)

type Profile struct {
	Username    string
	Name        string
	Description string
	GamesTotal  int
	Avatar      string
	Level       int
	Groups      []group.Group
	URL         string
	Friends     []friend.Friend
	Games       []game.Game
}

func (p *Profile) GetUsername(data string) {
	if strings.Contains(data, `actual_persona_name`) && p.Username == "" {
		replacer := strings.NewReplacer(`<span class="actual_persona_name">`, "", `</span>`, "", "	", "")
		p.Username = replacer.Replace(data)
	}
}

func (p *Profile) GetDescription(data string) {
	if strings.Contains(data, `name="Description`) {
		replacer := strings.NewReplacer(`<meta name="Description" content="`, "", `">`, "", "	", "")
		p.Description = replacer.Replace(data)
	}
}

func (p *Profile) GetName(data string) {
	if strings.Contains(data, `<bdi>`) && !strings.Contains(data, `</bdi></a>`) && p.Name == "" {
		replacer := strings.NewReplacer(`</bdi>`, "", `<bdi>`, "", "	", "")
		p.Name = replacer.Replace(data)
	}
}

func (p *Profile) GetGamesTotal(data string) {
	if strings.Contains(data, `games owned`) {
		gamesStr := strings.TrimRight(data, `games owned" >`)
		gamesInt, err := strconv.Atoi(gamesStr)
		if err != nil {
			log.Printf("%v-%v - Error: %v", "profile", "GetGamesTotal", err)
		}
		p.GamesTotal = gamesInt
	}
}

func (p *Profile) GetAvatar(data string) {
	if strings.Contains(data, `playerAvatarAutoSizeInner`) {
		replacer := strings.NewReplacer(`<div class="playerAvatarAutoSizeInner"><img src="`, "", `"></div>`, "", "	", "")
		p.Avatar = replacer.Replace(data)
	}
}

func (p *Profile) GetLevel(data string) {
	if strings.Contains(data, `friendPlayerLevelNum`) && p.Level == 0 {
		replacer := strings.NewReplacer(`</span></div></div>`, "", "	", "")
		levelSplit := strings.Split(replacer.Replace(data), `<span class="friendPlayerLevelNum">`)
		levelStr := levelSplit[len(levelSplit)-1]
		levelInt, err := strconv.Atoi(levelStr)
		if err != nil {
			log.Printf("%v-%v - Error: %v", "profile", "GetLevel", err)
		}
		p.Level = levelInt
	}
}

func (p *Profile) GetURL(data string) {
	if strings.Contains(data, `<a class="menuitem" href="https://steamcommunity.com/login`) {
		array := strings.Split(data, `%2F`)
		replacer := strings.NewReplacer(`<a class="menuitem" href="https://steamcommunity.com/login/home/?goto=`, "", `">`, "", "	", "")
		URL := replacer.Replace(fmt.Sprintf("https://steamcommunity.com/%v/%v", array[0], array[1]))
		p.URL = URL
	}
}
